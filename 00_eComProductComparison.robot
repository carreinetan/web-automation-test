* Settings *
Library    SeleniumLibrary

* Variables *
${BROWSER}    Chrome
${search_results}  results for

* Test Cases *
Search iPhone 13 and compare prices on Etsy and eBay
    Open Browser    https://www.etsy.com/    ${BROWSER}
    Maximize Browser Window
    Input Text    id=global-enhancements-search-query    iPhone 13
    Click Button    //Button[@value='Search']
    Wait Until Page Contains Element    xpath=//div[@data-search-results]
    ${etsy_products}    Get WebElements    xpath=//div[contains(@class, 'listing-card')]
    ${etsy_prices}    Get WebElements    xpath=//span[@class='currency-value']
    Open Browser    https://www.ebay.com/    ${BROWSER}
    Maximize Browser Window
    Input Text    name=_nkw    iPhone 13
    Click Button    //input[@value='Search']
#    Page Should Contain  ${search_results} iphone 13
#    Wait Until Page Contains Element    //h1[contains(text(), 'results for') and contains(text(), 'iPhone 13')]
    ${ebay_products}    Get WebElements    css=.s-item__title
    ${ebay_prices}    Get WebElements    css=.s-item__price
    ${results}    Create List
    FOR    ${i}    IN RANGE    ${etsy_products._len_()}
       ${etsy_product_name}  =  Get Text    ${etsy_products[${i}]}.//h3[contains(@class, 'text-gray')]/text()
       ${etsy_product_price}  =  Get Text    ${etsy_prices[${i}]}
       ${etsy_product_link}  =  Get Element Attribute    ${etsy_products[${i}]}.//a[contains(@class, 'text-gray') and contains(@class, 'underline-hover')]/@href
       Append To List    ${results}  =   ['Etsy', ${etsy_product_name}, ${etsy_product_price}, ${etsy_product_link}]
    END
    FOR    ${i}    IN RANGE    ${ebay_products._len_()}
       ${ebay_product_name}  =  Get Text    ${ebay_products[${i}]}
       ${ebay_product_price}  =  Get Text    ${ebay_prices[${i}]}
       ${ebay_product_link}  =  Get Element Attribute    ${ebay_products[${i}]}    href
       Append To List    ${results}  =  ['eBay', ${ebay_product_name}, ${ebay_product_price}, ${ebay_product_link}]
    END
    ${results_sorted}    Sort List    ${results}    key=lambda x: x[2]
    Log Many    ${results_sorted}