# Web Automation task

cd existing_repo
git remote add origin https://gitlab.com/automation6805415/mobile-automation-task.git
git branch -M main
git push -uf origin main


## Getting started

After git clone to your PC, make sure Robot Framework is installed properly in your PC.

This is a guide on how to install:
https://www.udemy.com/tutorial/robot-frameworkride-selenium-step-by-step-for-beginners/getting-started-with-robot-framework-step-by-step/

## Run robot command

Go to terminal and type robot 00_eComProductComparison.robot

Browsers of chrome will be prompted, navigating into Etsy and eBay websites.